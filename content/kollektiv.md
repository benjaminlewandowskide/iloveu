---
title: "Kollektiv"
date: "2020-02-20"
---

Viel ist Passiert in den letzten {{ date('now').diff((date('2011-1-1'))).format('%y') }} Jahren. Vom Veranstalten Selbst-verwalteter Events im Frankfurter umland auf Felder sowie unter brücken bis hin zu Club-Events mit Hunderten Besuchern oder Kooperationen mit anderen Crews und Vereinen. Wir treten ein für selbstverwaltetes Feiern, für kostengünstige (Jugend)-Kultur ohne Repression durch Polizei etc. Unsere Mitglieder wechseln der Kern blieb jedoch seit Jahren der Selbe.
