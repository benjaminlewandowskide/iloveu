---
title: "home"
slogan: "Rave is our passion"
date: "2020-02-20"
---

[![Facebook](/icons/facebook-f.svg){.icon} Facebook](http://facebook.com){.icon-link}
[![Mixcloud](/icons/mixcloud.svg){.icon} Mixcloud](http://facebook.com){.icon-link}
[![Instagram](/icons/instagram.svg){.icon} Instagram](http://facebook.com){.icon-link}

[![Events](/icons/calendar-alt.svg){.icon} Events](/events){.icon-link .btn--ghost}

[![Events](/icons/calendar-alt.svg){.icon} Events](/events){.icon-link}
[![Sound](/icons/compact-disc.svg){.icon} Sound](/events){.icon-link}
[![Kollektiv](/icons/users.svg){.icon} Kollektiv](/kollektiv){.icon-link}
[![Gallerie](/icons/calendar-alt.svg){.icon} Gallery](/events){.icon-link}
[![Home](/icons/home-alt.svg){.icon} Home](/events){.icon-link}
[![Impressum](/icons/home-alt.svg){.icon} Impressum](/imprint){.icon-link}
[![Kontakt](/icons/pen-fancy.svg){.icon} Kontakt](/kontakt){.icon-link}

Die <span class="logo" markdown="1">ILOVEU **DJCREW**</span> ist ein [<i class="fad fa-users"></i> Kollektiv](/kollektiv) von DJs aus dem Rhein-Main Gebiet. Seit 2011 veranstalten wir kostendeckend ausgelegte [<i class="fad fa-calendar-alt"></i> Club- und Outdoor Events](/events). Unser [<i class="fad fa-compact-disc"></i> Sound](/sound) ist elektronisch und deckt unter anderem House, Techhouse & Techno ab. Zu den Zielen gehört [<i class="fad fa-camera-retro"></i> selbstverwaltetes, gemeinschaftliches Feiern](/gallery) in der frankfurter Umgebung sowie ein freundschaftlicher, kollegialer Umgang mit dem Puplikum.

<span class="slogan">In diesem Sinne: Rave on</span>

Bei den vielen [<i class="fad fa-camera-retro"></i> CLUB- UND OUTDOOR EVENTS](/events) die wir als Rave Crew veranstalten entstehen regelmässig Audiomitschnitte die wir natürlich nicht für uns behalten wollen. Sie sind sowohl auf [<i class="fab fa-mixcloud"></i> mixcloud](https://www.mixcloud.com/iloveucrew/) als auch in der Rubrik [<i class="fad fa-compact-disc"></i> Sound](/sound) zu hören.
<span class="slogan">Dance like no one is watching</span>

Zu guten [<i class="fad fa-calendar-alt"></i> CLUB- UND OUTDOOR EVENTS](/events) gehört neben dem bombastischen [<i class="fad fa-compact-disc"></i> Sound](/sound) natürlich auch Deko, Licht und tanzende Massen. Diese Momente werden ab und zu in Bildform festgehalten und dann in der [<i class="fad fa-camera-retro"></i> Gallery](/gallery) sowie auf [<i class="fab fa-instagram"></i> instagram](https://www.instagram.com/iloveu.djcrew/)
von euch und mit euch geteilt.

<span class="slogan">A pictrue tells more than a thousand words</span>

Brennt dir etwas auf dem Herzen das du uns gerne mitteilen möchtest? Hast du auf einem der [<i class="fad fa-calendar-alt"></i> Events](/events) [<i class="fad fa-camera-retro"></i> Fotos](/gallery) gemacht und hättest gerne das wir es mit der Welt teilen? Du hast seit dem Letzten Rave die Frage welcher [<i class="fad fa-compact-disc"></i> Track](/sound) von einem DJ aus unserem [<i class="fad fa-users"></i> Kollektiv](/kollektiv) gespielt wurde?
