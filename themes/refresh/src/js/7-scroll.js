let scrollpos = window.scrollY;
const header = document.getElementById('mainhead');
const vh90 = window.innerHeight * 0.9;

function convertRemToPixels(rem) {
    return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

function remove_heroclass_on_scroll() {
    header.classList.remove('herohead');
}

function add_heroclass_on_scroll() {
    header.classList.add('herohead');
}

function scrollDetect() {
    let lastScroll = 0;

    window.onscroll = function() {
        let currentScroll = document.documentElement.scrollTop || document.body.scrollTop;

        if (currentScroll > vh90 && lastScroll <= currentScroll) {
            lastScroll = currentScroll;
            header.classList.add('fade-out');
        } else {
            lastScroll = currentScroll;
            header.classList.remove('fade-out');
        }
    };
}

window.addEventListener('scroll', function() {
    scrollpos = window.scrollY;

    if (scrollpos > convertRemToPixels(1)) {
        remove_heroclass_on_scroll();
    } else {
        add_heroclass_on_scroll();
    }
});

scrollDetect();

function scrollRemoveOnMenu() {
    if (document.getElementById('mainnav-toggle-input').checked === true) {
        document.getElementById('mainhead').classList.add('nav-open');
        document.getElementById('mainnav-toggle').innerHTML = '⨯';
    } else {
        document.getElementById('mainhead').classList.remove('nav-open');
        document.getElementById('mainnav-toggle').innerHTML = '≡';
    }
}

document.getElementById('mainnav-toggle-input').addEventListener('click', scrollRemoveOnMenu);
