module.exports = {
    plugins: [
        require("postcss-import"),
        require("postcss-url"),
        require("postcss-sorting")({
            "properties-order": "alphabetical",
            "unspecified-properties-position": "bottom"
        }),
        require("postcss-preset-env")({
            stage: 0
        }),
        require("cssnano")({
            preset: [
                "default",
                {
                    discardComments: {
                        removeAll: true,
                        mergeLonghand: false
                    }
                }
            ]
        })
    ]
};
